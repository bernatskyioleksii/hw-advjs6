// Теоретичне питання
// Поясніть своїми словами, як ви розумієте поняття асинхронності у Javascript

// Асинхронність у JavaScript означає, що певні операції можуть виконуватись паралельно без очікування на завершення попередніх операцій.


// Завдання
// Написати програму "Я тебе знайду по IP"
//
// Технічні вимоги:
//     Створити просту HTML-сторінку з кнопкою Знайти по IP.
//     Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
//     Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
//     під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
//     Усі запити на сервер необхідно виконати за допомогою async await.


async function fetchIpAddress() {
    try {
        const response = await fetch('https://api.ipify.org/?format=json');
        const data = await response.json();
        return data.ip;
    } catch (error) {
        console.error('Помилка отримання IP адреси:', error);
    }
}

async function fetchIpInformation(ip) {
    try {
        const response = await fetch(`https://ipapi.co/${ip}/json/`);
        const data = await response.json();
        console.log(data)
        return {
            continent: data.continent_code,
            country: data.country_name,
            region: data.region,
            city: data.city,
            district: data.country_area
        };

    } catch (error) {
        console.error('Помилка отримання інформації про IP:', error);
    }
}

document.getElementById('findIpButton').addEventListener('click', async () => {
    const ipAddress = await fetchIpAddress();
    const ipInformation = await fetchIpInformation(ipAddress);

    const resultElement = document.getElementById('result');
    resultElement.innerHTML = `
        <p><strong>Континент:</strong> ${ipInformation.continent}</p>
        <p><strong>Країна:</strong> ${ipInformation.country}</p>
        <p><strong>Регіон:</strong> ${ipInformation.region}</p>
        <p><strong>Місто:</strong> ${ipInformation.city}</p>
        <p><strong>Район:</strong> ${ipInformation.district}</p>
      `;
});

